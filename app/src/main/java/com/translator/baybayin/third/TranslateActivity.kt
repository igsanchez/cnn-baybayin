package com.translator.baybayin.third

import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.View.OnFocusChangeListener
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_translate.*


class TranslateActivity : AppCompatActivity() {
    private var isGoogleTranslate: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_translate)

        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

            override fun afterTextChanged(text: Editable?) {
                if (text != null) {
                    translate(text)
                }

            }
        })
        translatedTextView.onFocusChangeListener = OnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                translatedTextView.typeface = Typeface.DEFAULT
                translatedTextView.setText(this.shownText)
            } else {
                convertBaybayin(this.shownText)
            }
        }
    }

    fun translate(text: Editable) {
        if (!isGoogleTranslate) {
            shownText = text.toString()
            if (shownText.contains("[FfJjZzXxCcVvQq]".toRegex())) {
                shownText = shownText.replace("Q", "")
                        .replace("F", "")
                        .replace("J", "")
                        .replace("Z", "")
                        .replace("X", "")
                        .replace("C", "")
                        .replace("V", "")
                        .replace("q", "")
                        .replace("f", "")
                        .replace("j", "")
                        .replace("z", "")
                        .replace("x", "")
                        .replace("c", "")
                        .replace("v", "")
                editText.setText(shownText)
            } else {
                editText.setSelection(text.toString().length)
                convertBaybayin(shownText)
            }

        } else {
            googleTranslateTask().execute(shownText)
        }
    }

    var changing = false

    fun convertBaybayin(text: String) {

        var text1 = text.toLowerCase()
                .replace("ng", "N")

        text1 = "[bkdrghlmnNpstwy](?![aeiou])".toRegex().replace(text1) { res ->
            "${res.value}+"
        }
        text1 = "(?<![bkdrghlmnNpstwy])[aeiou]".toRegex().replace(text1) { res ->
            res.value.toUpperCase()
        }.replace("ba", "b").replace("ka", "k")
                .replace("da", "d").replace("ra", "r")
                .replace("ga", "g").replace("ha", "h")
                .replace("la", "l").replace("ma", "m")
                .replace("na", "n").replace("pa", "p")
                .replace("sa", "s").replace("ta", "t")
                .replace("wa", "w").replace("ya", "y")
                .replace("Na", "N")


        translatedTextView.setText(text1.replace("\\u200b", ""))
        translatedTextView.typeface = Typeface.createFromAsset(assets, "fonts/TagDoc93.ttf")
    }

    fun openCamera(view: View) {
        val cameraIntent = Intent(this, PreviewActivity::class.java)
        startActivity(cameraIntent)
    }

    lateinit var shownText: String


    fun enableTranslation(view: View) {
        if (gtranslate.isChecked) {
            val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = cm.activeNetworkInfo
            val isConnected: Boolean = activeNetwork?.isConnectedOrConnecting == true

            setTranslate(isConnected)
            if (!isConnected) {
                Toast.makeText(this, "Internet connection is required to enable Google Translate",
                        Toast.LENGTH_LONG).show()
            }
        } else {
            setTranslate(gtranslate.isChecked)
        }
        if (editText.text.isNotEmpty())
            translate(editText.text)
    }

    fun setTranslate(isTranslateOn: Boolean) {
        isGoogleTranslate = isTranslateOn
        gtranslate.isChecked = isTranslateOn
    }

    private inner class googleTranslateTask : AsyncTask<String, Void, Any>() {
        override fun doInBackground(vararg args: String): Any {
            val translator = GoogleTranslate("AIzaSyCBGLR4XozyZ2uyIg_5gz6fAZp6wYplsVQ")
            translator.translate(args[0], "en", "tl")
            val translation = translator.translate(args[0])
            return translation
        }

        override fun onPostExecute(result: Any) {
            shownText = result as String
            convertBaybayin(shownText)
        }
    }
}

