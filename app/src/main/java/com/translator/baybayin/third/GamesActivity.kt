package com.translator.baybayin.third

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import java.util.logging.Logger

class GamesActivity : AppCompatActivity() {
    val logger = Logger.getLogger("foo")
    var games = ArrayList<Game>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_games)
        initGames();
    }

    fun firstGame(view: View) {
        startGame(games.first());
    }

    fun initGames() {
        games.add(Game(R.drawable.ako_question, R.drawable.ako_wrong1, R.drawable.ako_correct, R.drawable.ako_wrong2, 2, 5))
        games.add(Game(R.drawable.ang_question, R.drawable.ang_correct, R.drawable.ang_wrong1, R.drawable.ang_wrong2, 1, 2))
        games.add(Game(R.drawable.babae_question, R.drawable.babae_correct, R.drawable.babae_wrong1, R.drawable.babae_wrong2, 1, 3))
        games.add(Game(R.drawable.bansa_question, R.drawable.bansa_wrong1, R.drawable.bansa_wrong2, R.drawable.bansa_correct, 3, 4))
        games.add(Game(R.drawable.lalaki_question, R.drawable.lalaki_wrong1, R.drawable.lalaki_correct, R.drawable.lalaki_wrong2, 2, 5))
        games.add(Game(R.drawable.mansanas_question, R.drawable.mansanas_wrong1, R.drawable.mansanas_wrong2, R.drawable.mansanas_correct, R.drawable.mansanas_wrong3, 3, 6))
        games.add(Game(R.drawable.ngipin_question, R.drawable.ngipin_wrong1, R.drawable.ngipin_wrong2, R.drawable.ngipin_correct, 3, 7))
        games.add(Game(R.drawable.pagong_question, R.drawable.pagong_wrong1, R.drawable.pagong_correct, R.drawable.pagong_wrong2, R.drawable.pagong_wrong3, 2, 8))
        games.add(Game(R.drawable.tv_question, R.drawable.tv_wrong1, R.drawable.tv_wrong2, R.drawable.tv_correct, R.drawable.tv_wrong3, 3, 9))
        games.add(Game(R.drawable.sagisag_question, R.drawable.sagisag_wrong1, R.drawable.sagisag_wrong2, R.drawable.sagisag_correct, 3, 10))
        games.add(Game(R.drawable.pilipino_question, R.drawable.pilipino_wrong1, R.drawable.pilipino_correct, R.drawable.pilipino_wrong2, 2, 11))
        games.add(Game(R.drawable.probinsya_question, R.drawable.probinsya_correct, R.drawable.probinsya_wrong1, R.drawable.probinsya_wrong2, 1, 12))
        games.add(Game(R.drawable.kayumanggi_question, R.drawable.kayumanggi_correct, R.drawable.kayumanggi_wrong1, R.drawable.kayumanggi_wrong2, 1, 13))
        games.add(Game(R.drawable.bahay_kubo_question, R.drawable.bahay_kubo_wrong1, R.drawable.bahay_kubo_wrong2, R.drawable.bahay_kubo_correct, 3, 14))

    }

    fun startGame(data: Game) {
        val isPicsGame: Boolean = data.button4 != 0
        val game: Intent
        if (isPicsGame) {
            game = Intent(this, GameTemplate2::class.java)
            game.putExtra("b4", data.button4)
        } else {
            game = Intent(this, GameTemplate::class.java)
        }
        game.putExtra("bg", data.background)
        game.putExtra("correctButton", data.correctButton)
        game.putExtra("b1", data.button1)
        game.putExtra("b2", data.button2)
        game.putExtra("b3", data.button3)

        startActivityForResult(game, data.roundFlag)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val isCorrect = data?.getBooleanExtra("isCorrect", false);
        if (isCorrect == true) {
            when (requestCode) {
                1 -> startGame(games[1]);
                2 -> startGame(games[2]);
                3 -> startGame(games[3]);
                4 -> startGame(games[4]);
                5 -> startGame(games[5]);
                6 -> startGame(games[6]);
                7 -> startGame(games[7]);
                8 -> startGame(games[8]);
                9 -> startGame(games[9]);
                10 -> startGame(games[10]);
                11 -> startGame(games[11]);
                12 -> startGame(games[12]);
                13 -> startGame(games[13]);
                14 -> {
                    val nextIntent = Intent(this, ScoreActivity::class.java)
                    startActivity(nextIntent)
                }
            }
        }

    }
}
