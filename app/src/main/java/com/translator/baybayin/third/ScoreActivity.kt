package com.translator.baybayin.third

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageButton

class ScoreActivity : AppCompatActivity() {

    private lateinit var btnH: ImageButton
    private lateinit var btnR: ImageButton
    private lateinit var btnQ: ImageButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_score)

        btnH = findViewById(R.id.btnHome) as ImageButton
        btnR = findViewById(R.id.btnRetry) as ImageButton
        btnQ = findViewById(R.id.btnQuit) as ImageButton
    }

    fun onClick(view: View) {
        when (view.getId()) {
            R.id.btnHome -> {
                val nextIntent = Intent(this, MainActivity::class.java)
                startActivity(nextIntent)
            }
            R.id.btnRetry -> {
                val nextIntent = Intent(this, GamesActivity::class.java)
                startActivity(nextIntent)
            }
            R.id.btnQuit -> {
                finish()
                moveTaskToBack(true)
            }
        }
    }
}
