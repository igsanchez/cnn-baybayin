package com.translator.baybayin.third

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_preview.*
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.nio.charset.Charset
import java.util.*


class PreviewActivity : AppCompatActivity() {

    val CAMERA_REQUEST = 0
    val RC_PERMISSIONS = 10
    val CROP_PIC = 2
    private lateinit var picUri: Uri

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_preview)

        val permissions = arrayOf("android.permission.WRITE_EXTERNAL_STORAGE")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, RC_PERMISSIONS)
        }

        val openCam = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (openCam.resolveActivity(packageManager) != null) {
            startActivityForResult(openCam, CAMERA_REQUEST)
        }

    }

    private fun performCrop() {
        // take care of exceptions
        try {
            // call the standard crop action intent (the user device may not
            // support it)
            val cropIntent = Intent("com.android.camera.action.CROP")
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*")
            // set crop properties
            cropIntent.putExtra("crop", "true")
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 2)
            cropIntent.putExtra("aspectY", 1)
            // indicate output X and Y
            cropIntent.putExtra("outputX", 256)
            cropIntent.putExtra("outputY", 256)
            // retrieve data on return
            cropIntent.putExtra("return-data", true)
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_PIC)
        } catch (anfe: ActivityNotFoundException) {
            val toast = Toast
                    .makeText(this, "This device doesn't support the crop action!", Toast.LENGTH_SHORT)
            toast.show()
        }
        // respond to users whose devices do not support the crop action
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST && data != null) {
                val imageBitmap = data.extras.get("data") as Bitmap
                uploadFileToServerTask().execute(saveBitmap(imageBitmap))
                imageViewer.setImageBitmap(imageBitmap)
//                picUri = Uri.parse(saveBitmap(imageBitmap))
//                performCrop()
            } else if (requestCode == CROP_PIC && data != null) {
                val imageBitmap = data.extras.get("data") as Bitmap


//                val classifier = ImageClassifier(this)
//                Toast.makeText(getApplicationContext(), classifier.classifyFrame(imageBitmap),
//                        Toast.LENGTH_LONG).show();
            }

        } else {
            finish()
        }
    }

    fun saveBitmap(bitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val destination = File(Environment.getExternalStorageDirectory(), "temp.jpg")
        val fo: FileOutputStream
        try {
            fo = FileOutputStream(destination)
            fo.write(bytes.toByteArray())
            fo.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
//        imageViewer.setImageBitmap(bitmap)
        return destination.getAbsolutePath()

    }

    private inner class uploadFileToServerTask : AsyncTask<String, String, Any>() {
        override fun doInBackground(vararg args: String): Any {
            try {
                val lineEnd = "\r\n"
                val twoHyphens = "--"
                val boundary = "*****"
                var bytesRead: Int
                var bytesAvailable: Int
                var bufferSize: Int
                var buffer: ByteArray
                val maxBufferSize = 1 * 1024 * 1024


                val url = URL("https://baybayinclassifier.herokuapp.com/classify")
                val connection = url.openConnection() as HttpURLConnection

                // Allow Inputs &amp; Outputs.
                connection.setDoInput(true)
                connection.setDoOutput(true)
                connection.setUseCaches(false)

                // Set HTTP method to POST.
                connection.setRequestMethod("POST")

                connection.setRequestProperty("Connection", "Keep-Alive")
                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=$boundary")

                var fileInputStream: FileInputStream
                var outputStream: DataOutputStream
                run {
                    outputStream = DataOutputStream(connection.getOutputStream())

                    outputStream.writeBytes(twoHyphens + boundary + lineEnd)
                    val filename = args[0]
                    outputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"$filename\"$lineEnd")
                    outputStream.writeBytes(lineEnd)

                    fileInputStream = FileInputStream(filename)

                    bytesAvailable = fileInputStream.available()
                    bufferSize = Math.min(bytesAvailable, maxBufferSize)

                    buffer = ByteArray(bufferSize)

                    // Read file
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize)

                    while (bytesRead > 0) {
                        outputStream.write(buffer, 0, bufferSize)
                        bytesAvailable = fileInputStream.available()
                        bufferSize = Math.min(bytesAvailable, maxBufferSize)
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize)
                    }
                    outputStream.writeBytes(lineEnd)
                    outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd)


                    val serverResponseCode = connection.getResponseCode()
                    val serverResponseMessage = connection.getResponseMessage()


                    fileInputStream.close()
                    outputStream.flush()
                    outputStream.close()

                    if (serverResponseCode == 200) {
                        return connection.getContent()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return "false"
        }

        override fun onPostExecute(result: Any) {
            if (result is InputStream) {
                textView.text = convert(result, Charset.defaultCharset())
            }

        }
    }

    @Throws(IOException::class)
    fun convert(inputStream: InputStream, charset: Charset): String {

        Scanner(inputStream, charset.name()).use({ scanner -> return scanner.useDelimiter("\\A").next() })
    }
}