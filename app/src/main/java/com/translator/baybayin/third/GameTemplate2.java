package com.translator.baybayin.third;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Random;

public class GameTemplate2 extends AppCompatActivity {
    private ImageButton btn1, btn2, btn3, btn4;
    private int correctButton = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_game_template2);
        Bundle extras = getIntent().getExtras();

//        Drawable bg = getResources().getDrawable(extras.getInt("bg"));
//        getWindow().getDecorView().setBackground(bg);
        getWindow().getDecorView().setBackgroundResource(extras.getInt("bg"));

        int correctButton = extras.getInt("correctButton");
        if (correctButton >= 1 && correctButton <= 3) {
            this.correctButton = correctButton;
        } else {
            this.correctButton = new Random().nextInt(3) + 1;
        }

        btn1 = (ImageButton) findViewById(R.id.btn1);
        btn2 = (ImageButton) findViewById(R.id.btn2);
        btn3 = (ImageButton) findViewById(R.id.btn3);
        btn4 = (ImageButton) findViewById(R.id.btn4);

        loadProperties(btn1, extras.getInt("b1"));
        loadProperties(btn2, extras.getInt("b2"));
        loadProperties(btn3, extras.getInt("b3"));
        loadProperties(btn4, extras.getInt("b4"));
    }

    private void loadProperties(ImageButton button, int imageResource) {
        button.setImageResource(imageResource);
        button.setOnClickListener(new WrongClickListener());
        if (correctButton == 1) {
            if (button == btn1) {
                button.setOnClickListener(new CorrectClickListener());
            }
        } else if (correctButton == 2) {
            if (button == btn2) {
                button.setOnClickListener(new CorrectClickListener());
            }
        } else if (correctButton == 3) {
            if (button == btn3) {
                button.setOnClickListener(new CorrectClickListener());
            }
        } else if (correctButton == 4) {
            if (button == btn4) {
                button.setOnClickListener(new CorrectClickListener());
            }
        }
    }


    private class CorrectClickListener implements View.OnClickListener {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "Your answer is Correct.", Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            data.putExtra("isCorrect", true);
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }

    private class WrongClickListener implements View.OnClickListener {
        public void onClick(View v) {
            Toast.makeText(getApplicationContext(), "WRONG!.", Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            data.putExtra("isCorrect", false);
            setResult(Activity.RESULT_OK, data);
            finish();
        }
    }
}
