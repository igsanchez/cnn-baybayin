package com.translator.baybayin.third;

public class Game {
    private int background;
    private int button1;
    private int button2;
    private int button3;
    private int button4;
    private int correctButton;
    private int roundFlag;

    public Game(int background, int button1, int button2, int button3, int correctButton, int roundFlag) {
        this.background = background;
        this.button1 = button1;
        this.button2 = button2;
        this.button3 = button3;
        this.correctButton = correctButton;

        this.roundFlag = roundFlag;
    }

    public Game(int background, int button1, int button2, int button3, int button4, int correctButton, int roundFlag) {
        this.background = background;
        this.button1 = button1;
        this.button2 = button2;
        this.button3 = button3;
        this.button4 = button4;
        this.correctButton = correctButton;
        this.roundFlag = roundFlag;
    }

    public int getBackground() {
        return background;
    }

    public void setBackground(int background) {
        this.background = background;
    }

    public int getButton1() {
        return button1;
    }

    public void setButton1(int button1) {
        this.button1 = button1;
    }

    public int getButton2() {
        return button2;
    }

    public void setButton2(int button2) {
        this.button2 = button2;
    }

    public int getButton3() {
        return button3;
    }

    public void setButton3(int button3) {
        this.button3 = button3;
    }

    public int getButton4() {
        return button4;
    }

    public void setButton4(int button4) {
        this.button4 = button4;
    }

    public int getCorrectButton() {
        return correctButton;
    }

    public void setCorrectButton(int correctButton) {
        this.correctButton = correctButton;
    }

    public int getRoundFlag() {
        return roundFlag;
    }

    public void setRoundFlag(int roundFlag) {
        this.roundFlag = roundFlag;
    }
}
