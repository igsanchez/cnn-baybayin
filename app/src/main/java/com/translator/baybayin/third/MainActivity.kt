package com.translator.baybayin.third

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }


    fun openTranslate(view: View) {
        val translateIntent = Intent(this, TranslateActivity::class.java)
        startActivity(translateIntent)
    }

    fun openTutorial(view: View) {
        val tutorialIntent = Intent(this, GuideActivity::class.java)
        startActivity(tutorialIntent)
    }

    fun openGames(view: View) {
        val gamesIntent = Intent(this, GamesActivity::class.java)
        startActivity(gamesIntent)
    }
}
